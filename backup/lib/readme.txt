The following dll's are required here.  Due to MSFT licensing issues they can't be
included in the repository.

Microsoft.SqlServer.ConnectionInfo.dll
Microsoft.SqlServer.Management.Sdk.Sfc.dll
Microsoft.SqlServer.Smo.dll
Microsoft.SqlServer.SqlClrProvider.dll
Microsoft.SqlServer.SqlEnum.dll

Note that if you have SQL Server installed on your build machine, VS will be smart 
enough to locate the assemblies, and the project will build.  However, they do need 
to be included in the project in order to be uploaded to the AppHarbor worker machine.