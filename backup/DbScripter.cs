﻿using System;
using System.Configuration;
using System.IO;
using Ionic.Zip;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace backup
{
    public class DbScripter
    {
        public void ScriptDb()
        {
            string tempfile = Path.GetTempPath() + "backup.sql";

            Server server = new Server(new ServerConnection
            {
                ConnectionString = ConfigurationManager.AppSettings["ConnectionString"]
            });
            Database db = server.Databases[ConfigurationManager.AppSettings["DbName"]];

            var tables = new Table[db.Tables.Count];
            db.Tables.CopyTo(tables, 0);

            var scripter = new Scripter(server);
            scripter.Options = new ScriptingOptions
            {
                ScriptDrops = true,
                WithDependencies = true,
                EnforceScriptingOptions = true,
                FileName = tempfile,
                ToFileOnly = true,
                DriAllConstraints = true
            };
            scripter.Script(tables);

            scripter.Options = new ScriptingOptions
            {
                ScriptDrops = false,
                ScriptSchema = true,
                ScriptData = true,
                DriAllConstraints = true,
                Indexes = true,
                WithDependencies = true,
                EnforceScriptingOptions = true,
                FileName = tempfile,
                ToFileOnly = true,
                AppendToFile = true
            };
            scripter.EnumScript(tables);



            using (var zip = new ZipFile())
            {
                zip.Name = string.Format(Path.GetTempPath() + "dbbackup-{0}.zip", DateTime.Now.ToString("yyyyMMddTHHmmss"));
                zip.AddFile(tempfile, "");
                zip.Save();

                BlobStorage.UploadFile(zip.Name);
            }

        }
    }
}
