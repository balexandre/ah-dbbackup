NOTES:

To build this project, first copy the SQL SMO dll's required to the lib folder.  
See the readme.txt file there.

Edit app.config with the details of your database, Amazon S3 account, and 
SMTP settings.

Currently the schedule is hard coded to run the backup once daily at 6am 
UTC (late night US time).

Backups will pile up on S3 indefinitely.  Would like to add code at some 
point to clean up old backups.

This project can be deployed as a background worker on Appharbor.